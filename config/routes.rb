# frozen_string_literal: true

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  resources :friends, only: %i[index show create update destroy]
  resources :tasks, only: %i[index show create update destroy]
end
