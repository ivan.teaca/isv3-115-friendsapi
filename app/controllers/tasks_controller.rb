# frozen_string_literal: true

class TasksController < ApplicationController
  def index
    @tasks = Task.all
    render json: @tasks
  end

  def show
    @task = Task.find(params[:id])
    render json: @task
  end

  def create
    @task = Task.create(
      language: params[:language],
      description: params[:description]
    )
    render json: @task
  end

  def update
    @task = Task.find(params[:id])
    @task.update(
      language: params[:language],
      description: params[:description]
    )
    render json: "#{@task.language} has ben updated!"
  end

  def destroy
    @tasks = Task.all
    @task = Task.find(params[:id])
    @task.destroy
    render json: "#{@task.language} has ben deleted!"
  end

  private

  def task_params
    params.require(:task).permit(:language, :description, :friend_id)
  end
end
