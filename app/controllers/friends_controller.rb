# frozen_string_literal: true

class FriendsController < ApplicationController
  def index
    @friends = Friend.all
    render json: @friends
  end

  def show
    @friend = Friend.find(params[:id])
    render json: @friend
  end

  def create
    @friend = Friend.create(
      name: params[:name],
      phone: params[:phone]
    )
    render json: @friend
  end

  def update
    @friend = Friend.find(params[:id])
    @friend.update(
      name: params[:name],
      phone: params[:phone]
    )
    render json: "#{@friends.name} has ben updated!"
  end

  def destroy
    @friend = Friend.find(params[:id])
    @friend.destroy
    render json: "#{@friends.name} has ben deleted!"
  end
end
