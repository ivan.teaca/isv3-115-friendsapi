# frozen_string_literal: true

friend1 = Friend.create(name: 'Jesse', phone: '+1 281 200')
Task.create(language: 'SQL', description: 'Create a new table', friend_id: friend1.id)

friend2 = Friend.create(name: 'Walter', phone: '+1 123 456')
Task.create(language: 'Python', description: 'Write a data analysis script', friend_id: friend2.id)

friend3 = Friend.create(name: 'Tuco', phone: '+1 555 789')
Task.create(language: 'JavaScript', description: 'Develop a responsive website', friend_id: friend3.id)

friend4 = Friend.create(name: 'Megan', phone: '+1 999 888')
Task.create(language: 'C++', description: 'Implement a sorting algorithm', friend_id: friend4.id)

friend5 = Friend.create(name: 'John', phone: '+1 777 333')
Task.create(language: 'Ruby', description: 'Build swagger doc for this API', friend_id: friend5.id)
