# frozen_string_literal: true

class AddFriendRefToTasks < ActiveRecord::Migration[7.0]
  def change
    add_reference :tasks, :friend, null: false, foreign_key: true
  end
end
