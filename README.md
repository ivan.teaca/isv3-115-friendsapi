### How to run Friends API:

1. Clone the repo and enter the project folder

```bash
git clone git@gitlab.com:ivan.teaca/isv3-115-friendsapi.git
```

2. Launch the application using docker-compose in detached mode:

```bash
docker-compose up -d
```

3. Push migrations:

```bash
docker-compose exec app rails db:migrate
```

and seeds:

```bash
docker-compose exec app rails db:seed
```


That's it. Now you can use it. Open:

http://0.0.0.0:3000/friends

http://0.0.0.0:3000/tasks

http://0.0.0.0:3000/friends/1

...

---
### Swagger doc available on:

http://0.0.0.0:3000/api-docs/index.html

---

To stop and remove the Docker containers, use the following command:

```
docker-compose down
```
